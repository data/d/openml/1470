# OpenML dataset: dresses-sales

https://www.openml.org/d/1470

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Muhammad Usman, Adeel Ahmed  
**Source**: UCI 

**Please cite**:   

Source:

Muhammad Usman & Adeel Ahmed, usman.madspot '@' gmail.com adeel.ahmed92 '@' gmail.com, Air University, Students at Air University.


Data Set Information:

Style, Price, Rating, Size, Season, NeckLine, SleeveLength, waiseline, Material,  FabricType, Decoration, Pattern, Type, Recommendation are Attributes in dataset. 


Attribute Information:

Style: Bohemia,brief,casual,cute,fashion,flare,novelty,OL,party,sexy,vintage,work. 
Price:Low,Average,Medium,High,Very-High 
Rating:1-5 
Size:S,M,L,XL,Free 
Season:Autumn,winter,Spring,Summer 
NeckLine:O-neck,backless,board-neck,Bowneck,halter,mandarin-collor,open,peterpan-collor,ruffled,scoop,slash-neck,square-collar,sweetheart,turndowncollar,V-neck. 
SleeveLength:full,half,halfsleeves,butterfly,sleveless,short,threequarter,turndown,null 
waiseline:dropped,empire,natural,princess,null. 
Material:wool,cotton,mix etc 
FabricType:shafoon,dobby,popline,satin,knitted,jersey,flannel,corduroy etc 
Decoration:applique,beading,bow,button,cascading,crystal,draped,embroridary,feathers,flowers etc 
Pattern type: solid,animal,dot,leapard etc 
Recommendation:0,1

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1470) of an [OpenML dataset](https://www.openml.org/d/1470). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1470/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1470/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1470/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

